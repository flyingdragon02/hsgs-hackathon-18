import React from "react";
import "./guide.less"

class Guide extends React.Component {
    render() {
        return(
            <div className="n38">
                <div className="guide">
                <h1> Hướng dẫn chơi </h1>
                <span>
                Hadi thích chơi ô số toán học với cha của mình, ông Aref. Vì thế, Hadi đã tạo ra một ô số cộng - cộng dưới đáy dành riêng
                cho ông Aref. Bạn có thể giúp ông Aref điền các số từ 1 đến n*n sao cho tổng các hàng và các cột bằng với số cho sẵn bên 
                phải hàng và bên dưới cột. (Với n là kích cỡ của bảng). 
                </span>
                <span>
                 <ul>
                    <li> Những ô<strong> hình vuông màu đỏ </strong> ở bên phải và bên dưới lần lượt là tổng phần còn thiếu của hàng và cột tương ứng. </li>
                    <li> Những ô<strong>  hình tròn màu xanh </strong> là những số cho trước. </li>
                    <li> Những ô <strong>  hình tròn màu vàng </strong> là những ô bạn phải điền số vào sao cho tất cá các số trong ô<strong>  hình 
                     vuông màu đỏ </strong> trở về 0. </li>
                 </ul>
                </span>
                </div>
            </div>
        );
    }
}

export default Guide;