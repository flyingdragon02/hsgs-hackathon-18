import React from "react";
import "./guide.less"

class Guide extends React.Component {
    render() {
        return(
            <div className="s43">
                <div className="guide">
                    <h1> Hướng dẫn chơi </h1>
                    <h2>Xoay để kết nối</h2>
                    <span>
                        Chaim vừa phát minh ra trò <strong> Xoay để kết nối </strong> này giống như một phiên bản hiện đại của loạt game
                        Solitaire truyền thông. Nhiệm vụ của người chơi là xoay các địa tròn lần lượt theo góc 90 độ sao cho tất cả hình 
                        vuông đều được nối với nhau bởi tập hợp các đường dọc và ngang.
                    </span>
                </div>
            </div>
        );
    }
}

export default Guide;